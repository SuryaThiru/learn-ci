import pytest
from learn_ci import *

@pytest.mark.parametrize("a,b,expected", [(3, 5, 8), (2, 4, 6), (6, 9, 15)])
def test_add(a, b, expected):
    assert add(a, b) == expected

@pytest.mark.parametrize("a,b,expected", [(3, 5, 8), (2, 4, 6), (6, 9, 15)])
def test_cadd(a, b, expected):
    assert c_add(a, b) == expected
